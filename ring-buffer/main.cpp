#include <atomic>
#include <array>
#include <chrono>
#include <cstddef>
#include <iostream>
#include <thread>
#include <type_traits>
#include <vector>

// Single-Producer/Single-Consumer Fixed-Size Ring Buffer (Queue)

#ifndef CAPACITY
#error "You should set -DCAPACITY=... when compiling"
#endif

template <typename T, size_t Capacity>
class SPSCRingBuffer {
 static_assert(std::is_trivial<T>::value);

 public:
  explicit SPSCRingBuffer() : buffer_(), tail_(0), head_(0) {
  }

  bool Publish(T value) {
    const size_t curr_head = head_.load(HeadMO(std::memory_order_acquire));
    const size_t curr_tail = tail_.load(std::memory_order_relaxed);

    if (IsFull(curr_head, curr_tail)) {
      return false;
    }

    buffer_[ToPos(curr_tail)].store(value, std::memory_order_relaxed);
    tail_.store(curr_tail + 1, TailMO(std::memory_order_release));
    return true;
  }

  bool Consume(T& value) {
    const size_t curr_head = head_.load(std::memory_order_relaxed);
    const size_t curr_tail = tail_.load(TailMO(std::memory_order_acquire));

    if (IsEmpty(curr_head, curr_tail)) {
      return false;
    }

    value = buffer_[ToPos(curr_head)].load(std::memory_order_relaxed);
    head_.store(curr_head + 1, HeadMO(std::memory_order_release));
    return true;
  }

 private:
  bool IsFull(const size_t head, const size_t tail) const {
    return tail - head + 1 > Capacity;
  }

  bool IsEmpty(const size_t head, const size_t tail) const {
    return tail == head;
  }

  size_t ToPos(const size_t slot) const {
    return slot % buffer_.size();
  }

  static std::memory_order HeadMO(std::memory_order mo) {
#ifdef HEAD_RELAXED
    return std::memory_order_relaxed;
#else
    return mo;
#endif
  }

  static std::memory_order TailMO(std::memory_order mo) {
#ifdef TAIL_RELAXED
    return std::memory_order_relaxed;
#else
    return mo;
#endif
  }

 private:
  std::array<std::atomic<T>, Capacity> buffer_;
  std::atomic<size_t> tail_;
  std::atomic<size_t> head_;
};

int main() {
  auto start = std::chrono::steady_clock::now();

  SPSCRingBuffer<uint64_t, CAPACITY> buf;
  constexpr uint64_t kValues = 100'000'000UL;

  // Single producer
  std::thread producer([&]() {
    for (uint64_t i = 0; i < kValues; ++i) {
      while (!buf.Publish(i)) {
        ;  // Retry
      }
    }
  });

  uint64_t total = 0;
  uint64_t count = 0;
  std::vector<uint64_t> value_counts(kValues, 0);

  // Single consumer
  std::thread consumer([&]() {
    for (uint64_t i = 0; i < kValues; ++i) {
      uint64_t value;
      while (!buf.Consume(value)) {
        ;  // Retry
      }
      if (value >= kValues) {
        std::cout << "Bogus value = " << value << std::endl;
      }
      total += value;
      ++count;
      ++value_counts[value];
    }
  });

  producer.join();
  consumer.join();

  auto end = std::chrono::steady_clock::now();
  std::chrono::duration<double> elapsed = end - start;

  constexpr uint64_t expected_total = (kValues - 1) * kValues / 2;
  if (expected_total == total) {
    std::cout << "All good, count = " << count;
  } else {
    std::cout << "Failure: got " << total << " instead of " << expected_total << ", count = " << count;
  }

  std::cout << ", elapsed = " << elapsed.count() << "s" << std::endl;
  uint64_t count_0 = 0;
  uint64_t count_2 = 0;
  for (size_t i = 0; i < kValues; ++i) {
    if (value_counts[i] == 0) {
      ++count_0;
    } else if (value_counts[i] > 1) {
      ++count_2;
    }
  }

  if (count_0 > 0) {
    std::cout << count_0 << " values never happened" << std::endl;
  }
  if (count_2 > 0) {
    std::cout << count_2 << " values happened multiple times" << std::endl;
  }

  return 0;
};
