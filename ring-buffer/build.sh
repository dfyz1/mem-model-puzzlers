#!/bin/sh

set -e

g++ -O2 -pthread -std=c++17 -DCAPACITY=1 -DHEAD_RELAXED=1 main.cpp -o ring_buffer_cap1_head_rel
g++ -O2 -pthread -std=c++17 -DCAPACITY=1 -DHEAD_RELAXED=1 -DTAIL_RELAXED=1 main.cpp -o ring_buffer_cap1_head_tail_rel
g++ -O2 -pthread -std=c++17 -DCAPACITY=256 -DHEAD_RELAXED=1 main.cpp -o ring_buffer_cap256_head_rel
g++ -O2 -pthread -std=c++17 -DCAPACITY=256 -DHEAD_RELAXED=1 -DTAIL_RELAXED=1 main.cpp -o ring_buffer_cap256_head_tail_rel
