#!/bin/sh

set -e

ITERS="${1:-5}"

for cap in 1 256
do
    for name in head head_tail
    do
        bin_name=./ring_buffer_cap${cap}_${name}_rel
        echo ${bin_name}
        for i in $(seq 1 ${ITERS})
        do
            $bin_name
        done
    done
done
