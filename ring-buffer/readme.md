## Циклический буфер без синхронизации в `head`

SPSC-очередь, в которой `acquire/release` расставлены только для `tail_`.
Для `head_` вся работа с атомиками происходит через `relaxed`.
В теории на процессорах со слабой моделью памяти (например, на `aarch64`) работать не должна, потому что могут потеряться какие-то записи. На aarch64 **сломать пока не удаётся**, но **удалось сломать** на DEC Alpha.

Картинка, поясняющая, как именно мы можем потерять записи (для `Capacity = 1`):
![lost stores](ring_buffer.jpeg)

Ассемблер приведён именно для `Capacity = 1`, для `Capacity = 256` (и других значений) он отличается чисто косметически.

Слева всегда ассемблер лямбды производителя, справа — ассемблер лямбды потребителя.
Оранжевым цветом показаны `acquire`-чтение и `release`-запись для `tail`.
Красным цветом показаны инструкции, которые должны участвовать в потере записей.

### Orange Pi One Plus

```
# lscpu
Architecture:                    aarch64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
CPU(s):                          4
[...]
Vendor ID:                       ARM
Model:                           4
Model name:                      Cortex-A53
[...]
# g++ --version
g++ (Debian 10.2.1-6) 10.2.1 20210110
[...]
```

![Orange Pi assembly](orange_pi.png)

### Apple M1

```
> sysctl machdep.cpu
[...]
machdep.cpu.core_count: 8
[...]
machdep.cpu.brand_string: Apple M1
> clang++ --version
Apple clang version 13.1.6 (clang-1316.0.21.2.3)
[...]
```

![M1 assembly](m1.png)


### X86-64

На `x86-64` всё должно работать даже с `relaxed`-атомиками и в `head_`, `tail_`.
Ассемблер просто для сравнения.

```
$ g++ --version
g++ (Ubuntu 11.2.0-7ubuntu2) 11.2.0
```

![X86](x86.png)


### DEC Alpha

На этой архитектуре неправильный буфер всё-таки ломается.

```
$ cat /proc/cpuinfo
cpu                     : Alpha
cpu model               : EV67
[...]
system type             : Tsunami
system variation        : DP264
[...]
platform string         : UP2000+ 666 MHz
cpus detected           : 2
cpus active             : 2
```

```
$ ./ring_buffer_borken
Failure: got 50000234561216 instead of 49999995000000, count = 10000000, elapsed = 5.58551s
825878 values never happened
825878 values happened multiple times
```

```
$ ./ring_buffer_correct
All good, count = 10000000, elapsed = 4.79024s
```

В диффе между сломанной и корректной версией видны дополнительные барьеры, без которых всё ломается.

```diff
--- ring_buffer_borken.txt	2022-05-12 00:01:13.000000000 +0300
+++ ring_buffer_correct.txt	2022-05-12 00:01:23.000000000 +0300
@@ -1,5 +1,5 @@
 
-ring_buffer_borken:     file format elf64-alpha
+ring_buffer_correct:     file format elf64-alpha
 
 
 Disassembly of section .init:
@@ -567,29 +567,29 @@
     17fc:	00 00 fe 2f 	unop	
 
 0000000000001800 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE_EEEEE6_M_runEv>:
-    1800:	0f 00 1f 25 	ldah	t7,15
+    1800:	0f 00 ff 24 	ldah	t6,15
     1804:	05 04 ff 47 	clr	t4
-    1808:	00 01 ff 20 	lda	t6,256
-    180c:	40 42 08 21 	lda	t7,16960(t7)
+    1808:	00 01 df 20 	lda	t5,256
+    180c:	40 42 e7 20 	lda	t6,16960(t6)
     1810:	08 00 70 a4 	ldq	t2,8(a0)
-    1814:	00 00 fe 2f 	unop	
-    1818:	1f 04 ff 47 	nop	
-    181c:	00 00 fe 2f 	unop	
-    1820:	08 08 23 a4 	ldq	t0,2056(t2)
-    1824:	00 08 43 a4 	ldq	t1,2048(t2)
-    1828:	01 00 82 20 	lda	t3,1(t1)
-    182c:	21 05 81 40 	subq	t3,t0,t0
-    1830:	a1 07 27 40 	cmpule	t0,t6,t0
-    1834:	fa ff 3f e4 	beq	t0,1820 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE_EEEEE6_M_runEv+0x20>
-    1838:	02 f0 5f 44 	and	t1,0xff,t1
-    183c:	42 06 43 40 	s8addq	t1,t2,t1
-    1840:	00 00 a2 b4 	stq	t4,0(t1)
-    1844:	01 00 a5 20 	lda	t4,1(t4)
-    1848:	00 40 00 60 	mb
-    184c:	a1 05 a8 40 	cmpeq	t4,t7,t0
-    1850:	00 08 83 b4 	stq	t3,2048(t2)
-    1854:	ee ff 3f e4 	beq	t0,1810 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE_EEEEE6_M_runEv+0x10>
-    1858:	01 80 fa 6b 	ret
+    1814:	08 08 23 a4 	ldq	t0,2056(t2)
+    1818:	00 40 00 60 	mb
+    181c:	00 08 43 a4 	ldq	t1,2048(t2)
+    1820:	01 00 82 20 	lda	t3,1(t1)
+    1824:	21 05 81 40 	subq	t3,t0,t0
+    1828:	a1 07 26 40 	cmpule	t0,t5,t0
+    182c:	f8 ff 3f e4 	beq	t0,1810 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE_EEEEE6_M_runEv+0x10>
+    1830:	02 f0 5f 44 	and	t1,0xff,t1
+    1834:	42 06 43 40 	s8addq	t1,t2,t1
+    1838:	00 00 a2 b4 	stq	t4,0(t1)
+    183c:	01 00 a5 20 	lda	t4,1(t4)
+    1840:	00 40 00 60 	mb
+    1844:	a1 05 a7 40 	cmpeq	t4,t6,t0
+    1848:	00 08 83 b4 	stq	t3,2048(t2)
+    184c:	f0 ff 3f e4 	beq	t0,1810 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE_EEEEE6_M_runEv+0x10>
+    1850:	01 80 fa 6b 	ret
+    1854:	00 00 fe 2f 	unop	
+    1858:	1f 04 ff 47 	nop	
     185c:	00 00 fe 2f 	unop	
 
 0000000000001860 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE_EEEEED1Ev>:
@@ -719,12 +719,12 @@
     1a2c:	43 06 62 40 	s8addq	t2,t1,t2
     1a30:	01 00 21 20 	lda	t0,1(t0)
     1a34:	00 00 83 a5 	ldq	s3,0(t2)
-    1a38:	08 08 22 b4 	stq	t0,2056(t1)
-    1a3c:	a1 07 8e 41 	cmpule	s3,s5,t0
-    1a40:	1f 00 20 e4 	beq	t0,1ac0 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE0_EEEEE6_M_runEv+0x100>
-    1a44:	10 00 6d a4 	ldq	t2,16(s4)
-    1a48:	20 00 8d a4 	ldq	t3,32(s4)
-    1a4c:	1f 04 ff 47 	nop	
+    1a38:	00 40 00 60 	mb
+    1a3c:	a3 07 8e 41 	cmpule	s3,s5,t2
+    1a40:	08 08 22 b4 	stq	t0,2056(t1)
+    1a44:	1e 00 60 e4 	beq	t2,1ac0 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE0_EEEEE6_M_runEv+0x100>
+    1a48:	10 00 6d a4 	ldq	t2,16(s4)
+    1a4c:	20 00 8d a4 	ldq	t3,32(s4)
     1a50:	ff ff ef 21 	lda	fp,-1(fp)
     1a54:	18 00 4d a4 	ldq	t1,24(s4)
     1a58:	00 00 23 a4 	ldq	t0,0(t2)
@@ -790,7 +790,7 @@
     1b48:	00 40 5b 6b 	jsr	ra,(t12),1b4c <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE0_EEEEE6_M_runEv+0x18c>
     1b4c:	02 00 ba 27 	ldah	gp,2(ra)
     1b50:	c4 a4 bd 23 	lda	gp,-23356(gp)
-    1b54:	bb ff ff c3 	br	1a44 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE0_EEEEE6_M_runEv+0x84>
+    1b54:	bc ff ff c3 	br	1a48 <_ZNSt6thread11_State_implINS_8_InvokerISt5tupleIJZ4mainEUlvE0_EEEEE6_M_runEv+0x88>
     1b58:	1f 04 ff 47 	nop	
     1b5c:	00 00 fe 2f 	unop	
     1b60:	10 04 e1 47 	mov	t0,a0
```
