cmake_minimum_required(VERSION 3.17)
project(mem_model_puzzlers)

set(CMAKE_CXX_STANDARD 20)

add_executable(ring_buffer main.cpp)
target_link_libraries(ring_buffer pthread)
