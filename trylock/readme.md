# try_lock

[Foundations of the C++ Concurrency Memory Model](https://www.hpl.hp.com/techreports/2008/HPL-2008-56.pdf)

## Java

https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/Lock.html

Unsuccessful locking and unlocking operations, and reentrant locking/unlocking operations, do not require any memory synchronization effects.